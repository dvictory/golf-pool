class Admin::EntriesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_only
  before_filter :set_admin

  before_action :set_entry, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    if params[:season].present?
      @entries = Season.find_by_name(params[:season]).entries.includes(:user)
    else
      @entries = Season.active.entries.includes(:user).all
    end
  end

  def show
    render 'entries/show'
  end

  def new
    @entry = Entry.new
    respond_with(@entry)
  end

  def edit
  end

  def update
    players = Array(params[:player_ids])
    players.each_with_index do |id, index|
      @entry.send("player#{index+1}_id=", id )
    end
    @entry.save
    respond_with(@entry)
  end

  def destroy
    @entry.destroy
    redirect_to admin_entries_path

  end

  private
    def set_entry
      @entry = Entry.find(params[:id])
    end

    def entry_params
      params[:entry]
    end

    def set_admin
      @admin = true
    end
end
