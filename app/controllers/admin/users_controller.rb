class Admin::UsersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_only

  respond_to :html

  def index
    @users = User.all
  end

  def update
    @user = User.find(params[:id])
    @user.update(user_params)
    redirect_to edit_admin_user_path(@user)
  end

  def edit
    @user = User.find(params[:id])
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    redirect_to admin_users_path
  end


  def user_params
    params.require(:user).permit(:username, :email, :password)
  end
end