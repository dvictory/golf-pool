class Admin::SeasonsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_only
  before_filter :set_admin

  before_action :set_season, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index

    @seasons = Season.order(:id).all

  end

  def new
    @season = Season.new
    respond_with(@season)
  end

  def create
    @season = Season.new(season_params)
    @season.save!
    redirect_to admin_seasons_path
  end

  def edit
  end

  def update

    @season.update_attributes(season_params)
    redirect_to admin_seasons_path
  end

  def destroy
    @season.destroy
    redirect_to admin_seasons_path

  end

  private
    def set_season
      @season = Season.find(params[:id])
    end

    def season_params
      params.require(:season).permit(:name, :active, :rules, :allow_entries)
    end

    def set_admin
      @admin = true
    end
end
