class HomeController < ApplicationController
  def index
    @season = Season.active
    redirect_to new_user_registration_path unless user_signed_in?
  end

  def demo
    render layout: false
  end
end