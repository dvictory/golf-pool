class PlayersController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_only, except: [:index]

  before_action :set_player, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @players = Season.active.players.order("salary desc")
    respond_with(@players)
  end

  def show
    respond_with(@player)
  end

  def new
    @player = Player.new
    respond_with(@player)
  end

  def edit
  end

  def create
    @player = Player.new(player_params)
    @player.save
    respond_with(@player)
  end

  def update
    @player.update(player_params)
    respond_with(@player)
  end

  def destroy
    @player.destroy
    respond_with(@player)
  end

  def import
    Player.import(params[:file])
    redirect_to players_path, notice: "Players imported."
  end

  private
    def set_player
      @player = Player.find(params[:id])
    end

    def player_params
      params[:player].permit(:name, :salary)
    end
end
