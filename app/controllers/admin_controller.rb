class AdminController < ApplicationController
  before_filter :authenticate_user!
  before_filter :admin_only

  def index

  end
  
  def import_tournaments
    Tournament.import(params[:file])
    redirect_to admin_path, notice: "Tournaments imported."
  end


end