class EntriesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :check_active, only: [:edit, :new, :create, :update]
  before_action :set_entry, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @entries = current_user.entries.all
    respond_with(@entries)
  end

  def show
    respond_with(@entry)
  end

  def new
    @entry = Entry.new
    respond_with(@entry)
  end

  def edit
    render :template => 'entries/new'
  end

  def create
    @entry = current_user.entries.build(entry_params)
    players = Array(params[:player_ids])
    players.each_with_index do |id, index|
      @entry.send("player#{index+1}_id=", id )
    end
    @entry.save
    respond_with(@entry)
  end

  def update
    players = Array(params[:player_ids])
    players.each_with_index do |id, index|
      @entry.send("player#{index+1}_id=", id )
    end
    @entry.save
    respond_with(@entry)
  end

  def destroy
    @entry.destroy
    respond_with(@entry)
  end

  def export
    @entries = Season.active.entries.all
    render xlsx: 'export'
  end

  private
    def set_entry
      @entry = current_user.entries.find(params[:id])
    end

    def entry_params
      params[:entry]
    end
end
