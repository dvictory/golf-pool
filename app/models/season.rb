class Season < ActiveRecord::Base
  has_many :entries
  has_many :players
  has_many :tournaments

  def self.active
    Season.where(active: true).first
  end

  def active=(value)
    Season.where("id != ?", id).update_all(active: false) if value
    write_attribute(:active, value)
  end
end
