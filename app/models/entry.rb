class Entry < ActiveRecord::Base
  belongs_to :user
  belongs_to :player1, class_name: 'Player'
  belongs_to :player2, class_name: 'Player'
  belongs_to :player3, class_name: 'Player'
  belongs_to :player4, class_name: 'Player'
  belongs_to :player5, class_name: 'Player'
  belongs_to :player6, class_name: 'Player'
  belongs_to :player7, class_name: 'Player'
  belongs_to :player8, class_name: 'Player'
  belongs_to :season
  before_validation :set_season, on: :create
  validates_presence_of :season

  def total
    sum = 0
    (1..8).each do |i|
      sum = sum + send("player#{i}").salary if send("player#{i}")
    end
    sum
  end

  def players
    players = []
    (1..8).each do |i|
      players << send("player#{i}") if send("player#{i}")
    end

    players
  end

  def set_season
    self.season ||= Season.active
  end
end
