require 'csv'

class Player < ActiveRecord::Base

  before_validation :set_season, on: :create
  validates_presence_of :name, :salary, :season
  belongs_to :season


  def self.import(file)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    (2..spreadsheet.last_row).each do |i|
      row = Hash[[header, spreadsheet.row(i)].transpose]
      next if row['name'].blank?
      player = find_by_id(row["id"]) || new
      player.id = row['id'] unless player.persisted?
      player.name = row['name'].split.map(&:capitalize).join(' ')
      player.salary = row['salary']
      player.season = Season.active
      player.save!
    end
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
    when ".csv" then Roo::Csv.new(file.path, nil, :ignore)
    when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
    when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
    else raise "Unknown file type: #{file.original_filename}"
    end
  end

  def set_season
    self.season ||= Season.active
  end
end
