// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require underscore
//= require angular/angular
//= require angular-resource/angular-resource
//= require angular-sanitize/angular-sanitize
//= require angular-ui-select/dist/select.js
//= require angular-devise/lib/devise
//= require bootstrap-sass-official/assets/javascripts/bootstrap
//= require_tree .

var golf_pool = angular.module('golf-pool',['Devise', 'ngResource', 'ngSanitize', 'ui.select']);


golf_pool.controller('NewEntry', ['$scope', '$location', 'Auth', 'Player', 'Entry', function($scope, $location, Auth, Player, Entry){
  //$scope.players = Player.query();
  
  Player.query(function(players){
    $scope.players = players;
    
    if($location.search().id !== undefined){
      $scope.entry = Entry.get({id: $location.search().id}, function() {
        loadPlayers($scope.entry);
      });
    
    }
  });
  
  
  $scope.selected_player = {};

  $scope.picks = [];

  $scope.addPlayer = function () {
    $scope.error = null;
    var exiting_player = _.find($scope.picks, function(player){
      return $scope.selected_player.selected == player.id
    });

    if(exiting_player) {
      $scope.error = "Player already selected";
      return
    };

    var player = _.find($scope.players, function(player){
      return $scope.selected_player.selected == player.id
    });

    $scope.picks.push(player);
  };

  $scope.total = function() {
    var sum = _.reduce($scope.picks, function(memo, num){
      return memo + num.salary;
    }, 0);
    return sum;
  };

  $scope.remaining = function() {
    return 7000000 - $scope.total();
  };

  $scope.createNew = function () {
    console.log("Submitting form...");
    var player_ids = _.map($scope.picks, function(pick){ return pick.id; });
//    var entry = new Entry({player_ids: player_ids});
//    entry.$save();
    if($scope.entry && $scope.entry.id !== undefined){
      Entry.update({id: $scope.entry.id}, {player_ids: player_ids}, function (entry) {
        window.location = '/entries';
      });
// ;      $scope.entry.$update({id: $scope.entry.id, player_ids: player_ids});
    }else{
      Entry.save({player_ids: player_ids}, function(user){
        window.location = '/entries';
      });
    }
    
  };

  $scope.removePlayer = function (id) {
    $scope.picks = _.without($scope.picks, _.findWhere($scope.picks, {id: id}));
  }

  function loadPlayers(entry){
    for(i=1; i<=8; i++){
      var player = _.find($scope.players, function(player){
        return entry["player" + i + "_id"] == player.id
      });
  
      $scope.picks.push(player);
    }
    
  }

}]);

golf_pool.factory('Player', ['$resource', function($resource) {
  return $resource('/players/:id.json', null,
    {
        'update': { method:'PUT' }
    });
}]);

golf_pool.factory('Entry', ['$resource', function($resource) {
  return $resource('/entries/:id.json', null,
    {
        'update': { method:'PUT' }
    });
}]);


var BrowserDetect = {
        init: function () {
            this.browser = this.searchString(this.dataBrowser) || "Other";
            this.version = this.searchVersion(navigator.userAgent) || this.searchVersion(navigator.appVersion) || "Unknown";
        },
        searchString: function (data) {
            for (var i = 0; i < data.length; i++) {
                var dataString = data[i].string;
                this.versionSearchString = data[i].subString;

                if (dataString.indexOf(data[i].subString) !== -1) {
                    return data[i].identity;
                }
            }
        },
        searchVersion: function (dataString) {
            var index = dataString.indexOf(this.versionSearchString);
            if (index === -1) {
                return;
            }

            var rv = dataString.indexOf("rv:");
            if (this.versionSearchString === "Trident" && rv !== -1) {
                return parseFloat(dataString.substring(rv + 3));
            } else {
                return parseFloat(dataString.substring(index + this.versionSearchString.length + 1));
            }
        },

        dataBrowser: [
            {string: navigator.userAgent, subString: "Chrome", identity: "Chrome"},
            {string: navigator.userAgent, subString: "MSIE", identity: "Explorer"},
            {string: navigator.userAgent, subString: "Trident", identity: "Explorer"},
            {string: navigator.userAgent, subString: "Firefox", identity: "Firefox"},
            {string: navigator.userAgent, subString: "Safari", identity: "Safari"},
            {string: navigator.userAgent, subString: "Opera", identity: "Opera"}
        ]

    };


$( document ).ready(function() {
  BrowserDetect.init();
  if(BrowserDetect.browser == 'Explorer'){
    $("#ie-warning").show();
  }

});

