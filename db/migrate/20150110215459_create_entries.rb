class CreateEntries < ActiveRecord::Migration
  def change
    create_table :entries do |t|
      t.integer :user_id
      t.string :season
      t.integer :player1_id
      t.integer :player2_id
      t.integer :player3_id
      t.integer :player4_id
      t.integer :player5_id
      t.integer :player6_id
      t.integer :player7_id
      t.integer :player8_id

      t.timestamps null: false
    end
  end
end
