class AddAllowEntriesToSeason < ActiveRecord::Migration
  def change
    add_column :seasons, :allow_entries, :boolean, default: false

  end
end
