class CreateTournaments < ActiveRecord::Migration
  def change
    create_table :tournaments do |t|
      t.integer :season_id
      t.string :name
      t.date :start
      t.string :week

      t.timestamps null: false
    end
  end
end
