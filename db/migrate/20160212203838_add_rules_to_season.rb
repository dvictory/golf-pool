class AddRulesToSeason < ActiveRecord::Migration
  def change
    add_column :seasons, :rules, :text
    Player.update_all(season_id: 1)
  end
end
