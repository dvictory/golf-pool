class CreateSeasons < ActiveRecord::Migration
  def change
    create_table :seasons do |t|
      t.string :name
      t.boolean :active

      t.timestamps null: false
    end

    add_column :entries, :season_id, :integer
    add_index :entries, :season_id

    s = Season.create(name: '2015')
    Entry.update_all(season_id: s.id)
  end
end
