class AddSeasontoPlayer < ActiveRecord::Migration
  def change
    add_column :players, :season_id, :integer
    add_index :players, :season_id
  end
end
